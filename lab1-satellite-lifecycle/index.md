# Lab 1: Red Hat Satellite for Content Lifecycle Management

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Lab 1: Red Hat Satellite for Content Lifecycle Management](#lab-1-red-hat-satellite-for-content-lifecycle-management)
	- [Goal of Lab](#goal-of-lab)
		- [Notes about Satellite Environment and Configurations](#notes-about-satellite-environment-and-configurations)
		- [Log into the environment](#log-into-the-environment)
		- [Verify Synced Content](#verify-synced-content)
		- [Create Lifecycle Environment](#create-lifecycle-environment)
		- [Create Content View](#create-content-view)

<!-- /TOC -->

## Goal of Lab

In this lab, you will be provided the necessary steps and background information on how to use Red Hat Satellite for content management with managed hosts. Some of the values may be pre-populated for you, and the pre-populated values may be required for subsequent labs, so do not remove the pre-populated values.

### Notes about Satellite Environment and Configurations

1.  Each checkpoint will have a pre-built/configured object (Activation Key, Content View, Lifecycle Environment, etc.).
	* These pre-built objects can be used as examples as you create your own.
	*  You’ll need to name your objects differently, however configurations will need to match, so we have these here for your reference.
2.  Lab Exercise Setup: Based on the structure/mobility of the lab, it’s possible to run into some issues with certain services therein. If you run into anything, you can run the following to fully restart the satellite services:

SSH from your jumpbox as outlined in Lab 0 to root@sat.example.com

    [lab-user@workstation \$] sudo -i
    [root@workstation \#] ssh sat.example.com
    [root@satellite \~]\# katello-service restart

### Log into the environment

1.  Access to the Satellite server from your browser: Although several browsers are supported, we recommend the use of Firefox. Remember, as previously mentioned, if you see an SSL warning, accept this for the lab  as its a self-signed certificate from the application.

Point your web browser to https://sat-\<GUID\>.rhpds.opentlc.com or click on your Lab GUID page that is open in your browser and open the link for your Sat Where \<GUID\> is your unique identifier as described in Lab 0.

When logging into these systems you could potentially receive the following due to a self signed certificate

![](images/image127.png)

And for Firefox…

![](images/image10.png)

Click “Advanced” to open the advanced menu, and then “Proceed to ….” or “Add Exception” and “Confirm Security Exception”  at the bottom of the dialogue.

![](images/image9.png)

The warning is due to a self signed certificate authority potentially not being available on your local workstation and if you’re using the Chrome browser.

Now that we’ve navigated to the login, login as user “admin” with the password “r3dh4t1!” as mentioned previously in this guide.

Let’s begin.

### Verify Synced Content

In this lab we will have a few items populated for us already, and we will also have to create some items. The scenario is that you’re the proud new beneficiary of another team’s Satellite server. Let’s look at some of the ways EXAMPLE.COM is using Satellite for content management.


1.  Navigate to, and confirm content Sync Status

    **NOTE**: To save time, in your lab all content is up to date and ready to continue through to next step.
    This is for information purposes only. No action is required from you for this step.

  * To Navigate to the Sync Status page, select “Content” from your toolbar, and select “Sync Status” from the dropdown.
  * In the top right corner, you can click “Expand All”, or you can simply expand each item in the tree manually to confirm which of the repositories is available and recently synced.

  ![](images/image115.jpg)

**Background**

In a live environment we want to ensure the content is available, and up to date. With that, we can ensure that when creating our Content View we’re publishing the latest available content so we can promote it through our Lifecycles to be used by our Content Hosts.

In the previous screenshot, you can see the content was not up to date, so a sync would be needed in order to get it up to date. To sync you would simply select the desired repository, and select “Synchronize” in the bottom right corner. (There is no need to perform this command. It is for informational purposes only and will only hinder your progress in
the lab.)

### Create Lifecycle Environment

1.  Navigate to Lifecycle Environments

	* To Navigate to the Lifecycle Environment page, select “Content” from your toolbar, and select “Lifecycle Environments” from the dropdown.
	* Since this is the first time we’re logging into our virtual environment, the page will take a moment to populate with the ‘Summit\_DEV’ and ‘Summit\_PROD” Lifecycle Environments in your Environment Path. Once loaded the “Add New Environment” button will appear for you to create the new Lifecycle Environment in your Environment Path.

	![](images/image112.jpg)

2.  Create New Environment

	* Click the “Add New Environment” button discovered in the previous step, and continue with naming the Lifecycle Environment whatever you’d like.
	**Note**: This is NOT the blue “Create Environment Path” button in the top right corner. In the previous step, this may take a moment to load.

 * Click “Save” to save the new environment.

![](images/image77.jpg)

**Background**

The most common environment use-case would be stage-releasing content through multiple environments in a lifecycle, before finally pushing out into production. The simplest illustration would be to first promote content to a Development environment. Once verified there, that same content can then be promoted out to a Live or QA Testing environment, and then finally out to Production.

Creating a Lifecycle Environment allows for the creation of this simple and efficient stage-releasing structure through which your Content Hosts can receive this content in each staging cycle.

**NOTE**  - Even without creating a new Lifecycle Environment, each Satellite server is pre-configured with a Library Environment which is effectively ALL synced content available on the Satellite. This environment is unchangeable, and you cannot add new environments to this path. That said, Content Views will promote to this Library Environment by default, and Content Hosts can be configured to pull directly from Library if desired.

### Create Content View

1.  Navigate to Content Views

	* To Navigate to the Content Views page, select “Content” from your toolbar, and select “Content Views” from the dropdown.
    ![](images/image8.jpg)

2.  Create the new Content View:

	*  Click the “Create New View” button discovered in the previous step, and continue with naming the Content View whatever you’d like.
	* Click “Save” to save the new view.
	![](images/image107.jpg)

3.  Add Content to New View

	* From the Content Views page, select your newly created Content View from the list.
**Note:** If continuing from the last step, you should be directed to this page automatically.

	* Select “Yum Content” from the Content View toolbar, and then select “Repositories” from the dropdown list. (Dropdown located under “Yum Content” option)
	* Select “Add” from the new section under Repository Selection. 
	* Click the checkbox next to each of the following repositories and then click “Add Repositories” in the top right corner of the Repository Selection section.

```
Red Hat CloudForms Management Engine 5.8 RPMs x86\_64
Red Hat Enterprise Linux 7 Server - Extras RPMs x86\_64
Red Hat Enterprise Linux 7 Server Kickstart x86\_64 7.4
Red Hat Enterprise Linux 7 Server - RH Common RPMs x86\_64 7Server
Red Hat Enterprise Linux 7 Server RPMs x86\_64 7Server
Red Hat Enterprise Linux 7 Server - Supplementary RPMs x86\_647Server 
Red Hat Satellite Tools 6.3 for RHEL 7 Server RPMs x86\_64
Red Hat Software Collections RPMs for Red Hat Enterprise Linux 7Server x86\_64 7Server 
```


![](images/image56.jpg)

4.  Confirm required repositories are present in Content View

	* Once added, you can confirm (and/or remove) the added repositories by clicking “List/Remove” in the Repository Selection section.

![](images/image44.jpg)

5.  Publish New Version of Content View

	* From the Content Views page, select your newly created Content View from the list (Content -\> Content Views -\> Whatever you named your Content View)
	* Select “Versions” from the Content View toolbar.
	* Click the blue “Publish New Version” button in the top right corner of the page to publish this new Content View.
	* Feel free to add a Description, and then click “Save” to lock the
    content in at this current state

**NOTE:** Publishing this Content View will take \~5 minutes to
    complete. Feel free to read ahead over the next steps, and/or over
    the Background section while waiting. \*\*\*

![](images/image74.jpg)

6.  Promote Content View through Lifecycle Environment

	* From the Content Views page, select your newly created Content View from the list.
	* Select “Versions” from the Content View toolbar.
	* Click “Promote” under the Actions column of the desired Version

    **NOTE:** The Starred Lifecycle Environment is representative of the “Next Environment” based on your designed Lifecycle Path, which creates an easy way to identify where you are currently posted in your Lifecycle, and what Environment is next in line.

	* Select your new Lifecycle Environment created in the last step.

  		**NOTE:** Promoting Content View should take \~3 minutes to complete As the final step of this exercise, you may move on to Background and to the next lab if you made it this far.

   		**NOTE:** If you receive an error about “Required lock is already taken by other running tasks” then your previous request (Content view publish) has not yet completed.

![](images/image63.jpg)

![](images/image117.jpg)

**Background**

As mentioned earlier, the most common environment use-case would be stage-releasing content through a number of environments in a lifecycle before pushing into production. Publishing a Content View allows you to lock in content at a certain point in time, allowing for intentional and accurate content control when ultimately promoting through your
lifecycle stages.

Beyond just locking content, further filtering can be applied to include/exclude specific packages/errata/puppet modules/etc., depending on your exact use-case and business needs.

Should any additional changes be made to a Content View, in order for them to take effect from the Content Hosts perspective, a new version must be published and promoted each time. (Note: Older versions of each publish will remain to allow for multi-staging and/or backups of known, stable versions.)


Continue to the next step: [Lab 2: Satellite for Content Host Management](../lab2-satellite-content/index.md)
